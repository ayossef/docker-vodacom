# Project Building Steps

## 1. Create front end image
Build the fronend docker file to create the image then push it to docker hub


## 2. Create the backend image
Build the backend docker file to create the image then push it to docker hub

## 3. Update the docker-compose file 
Update the docker compose file, adding two new services which are backend and fronent

## 4. Run the stack
Use docker compose up command to being the stack up